## app接口

> 高亮为完全暴露接口，其他为保护接口（需要进行token验证）

> x-access-token
>
> token验证过程：
>
> 第一次登陆之后会服务端会返回一个token字符串，这个token字符串是根据userId和当前时间签发的，过期时间设置为7天。app端需要做的就是每次登陆后获得这个token，并保存在本地，每次请求保护接口需要将token置于http请求头的x-access-token上，android如何设置请求头百度解决。每次注销的时候将本地的token删除即可。
>
> 服务端可以通过每次发送的token获得当前用户userId，用以处理一些操作。

> 根目录为：http://www.forus616.cn:3007
>
> 示例URL：http://www.forus616.cn:3007/userLogin

#### 用户信息接口

|               接口名               |          接口地址          | 请求方法 |                           接受参数                           |                           返回数据                           |
| :--------------------------------: | :------------------------: | :------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|            ==用户注册==            |     ==/userRegister==      | ==POST== |                     ==email、password==                      |                        ==成功/失败==                         |
|            ==用户登录==            |       ==/userLogin==       | ==POST== |                     ==email、password==                      |                    ==成功（token）/失败==                    |
|       用户注销（暂时不能用）       |        /userDelete         |   GET    |                            userId                            |                          成功/失败                           |
|    获取所有用户个人信息(全暴露)    |       /userInfoList        |   GET    |                              无                              |                       ↓数据组成的数组                        |
|        获取指定用户个人信息        |       /userInfoById        |   POST   |                       指定用户的userId                       |  userId、email、userName、sex、birthday、header、personInfo  |
|            修改用户信息            |      /userInfoUpdate       |   POST   | 要更改的数据（暂时需要提交全部数据包括：userName、sex、birthday、header、personInfo） |                          成功/失败                           |
| 获取该用户关注的所有人发的文章信息 | /userAttendUserArticleList |   Get    |                              无                              | [关注的人信息+该人发的文章，[关注的人信息+该人发的文章，...] |

上述接口完成本地测试2021-11-11 10:41:22

#### 帖子接口

|           接口名           |      接口地址       | 请求方法 |        接受参数        |      返回数据      |
| :------------------------: | :-----------------: | :------: | :--------------------: | :----------------: |
|          创建帖子          |   /articleCreate    |   POST   | title、body、tag、time |     成功/失败      |
|          删除帖子          |   /articleDelete    |   POST   |       articleId        |     成功/失败      |
| 获得所有审核通过的文章列表 | /articleCheckedList |   GET    |           无           | articleId、tags... |
|  获得用户自己的的文章列表  |    /articleList     |   GET    |           无           |        同上        |
|      获取指定Id的文章      |    /articleById     |   POST   |       articleId        |      文章信息      |

上述接口完成本地测试2021-11-11 11:13:55

#### 评论接口

|    接口名    |   接口地址    | 请求方法 |       接受参数        |                返回参数                 |
| :----------: | :-----------: | :------: | :-------------------: | :-------------------------------------: |
|   新增评论   | /reviewCreate |   POST   | articleId、body、time |                成功/失败                |
|   删除评论   | /reviewDelete |   POST   |  reviewId,articleId   |                成功/失败                |
| 获取所有评论 |  /reviewList  |   post   |       articleId       | reviewId、userId、articleId、body、time |
|              |               |          |                       |                                         |

上述接口完成本地测试2021-11-11 11:15:31

#### 用户关注收藏接口

|                接口名                |          接口地址          | 请求方法 |       接受参数       |                     返回数据                      |
| :----------------------------------: | :------------------------: | :------: | :------------------: | :-----------------------------------------------: |
|               新增关注               |        /userAttend         |   POST   |  attendUserId，time  |                     成功/失败                     |
|               取消关注               |       /userUnattend        |   POST   |     attendUserId     |                     成功/失败                     |
|             获取关注列表             |      /userAttendList       |   POST   |        userId        |                 关注用户信息数组                  |
|               新增收藏               |      /articleCollect       |   POST   |   articleId，time    |                     成功/失败                     |
|               取消收藏               |     /articleUncollect      |   POST   |      articleId       |                     成功/失败                     |
|        获取指定用户的收藏列表        |    /articleCollectList     |   POST   |        userId        |                 收藏文章信息数组                  |
|       获取某用户是否收藏某文章       |    /articleCollectFlag     |   POST   |  userId、articleId   |                    true、false                    |
|       获取某用户是否关注某用户       |      /userAttendFlag       |   POST   | userId、attendUserId |                    true、false                    |
| 获取当前用户关注的用户以及他们的文章 | /userAttendUserArticleList |   Get    |          无          | [关注人+该人发的文章，关注人+该人发的文章，.....] |

2021-11-14 21:32:30本地测试完成



|                    接口名                    |       接口地址       | 请求方法 | 接受参数 |                           返回数据                           |
| :------------------------------------------: | :------------------: | :------: | :------: | :----------------------------------------------------------: |
|             关注当前用户的所有人             |    /userFansList     |   get    |    无    |                           userList                           |
|       收藏当前用户文章的所有人和该文章       |   /articleFansList   |   get    |    无    |     [articleInfo+collecter、articleInfo+collecter、...]      |
| 评论当前用户文章的所有人和当前文章和评论内容 | /articleReviewerList |   get    |    无    | [articleInfo+reviewer+reviewInfo、articleInfo+reviewer+reviewInfo、...] |



## Web后台接口

#### 管理员登录结构

|         接口名         |    接口地址     | 请求方法 |      接受参数       |       返回数据       |
| :--------------------: | :-------------: | :------: | :-----------------: | :------------------: |
|     ==管理员登录==     | ==/adminLogin== | ==post== | ==email、password== | ==成功(token)/失败== |
|       新增管理员       |  /adminCreate   |   post   |   email、password   |      成功/失败       |
|       删除管理员       |   /adminCheck   |   Post   |                     |      成功/失败       |
| 获取管理员表的全部信息 |   /adminList    |   get    |         无          |         list         |

最后测试时间2021-11-11 11:22:43

#### 用户信息管理接口

|    接口名    |     接口地址      | 请求方法 |                  接受参数                   |   返回数据   |
| :----------: | :---------------: | :------: | :-----------------------------------------: | :----------: |
|   新增用户   |   /adUserCreate   |   post   |               email，password               |  成功/失败   |
|   删除用户   |   /adUserDelete   |   post   |                   userId                    |  成功/失败   |
| 获取用户列表 | /adUserInfoUpdate |   GET    |                     无                      | 用户信息数组 |
| 修改用户信息 | /manageUserUpdate |   POST   | userId、userName、sex、birthday、personInfo |  成功/失败   |

最后测试时间2021-11-11 11:26:31

#### 帖子信息管理接口

|      接口名      |    接口地址     | 请求方法 |    接受参数    |      返回数据      |
| :--------------: | :-------------: | :------: | :------------: | :----------------: |
|     删除帖子     | /articleDelete  |   GET    |   articleId    |     成功/失败      |
|   获取帖子列表   | /adArticleList  |   GET    |                | articleId、tags... |
| 修改帖子审核状态 | /adArticleCheck |   POST   | 修改之前的状态 |     成功/失败      |
|                  |                 |          |                |                    |

最后测试时间 2021-11-11 11:28:29

周一任务

​	完成三个接口的调试

​	在新增收藏新增关注接口 服务端写入时间time属性
