const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const moment = require('moment');// 生成token的框架
const testToken = require('../utils/tokenCheck');

// 新增关注
route.post('/userAttend', async (req, res) => {
    console.log('get:/userAttend')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 关注的人信息获取
        let userId = result.userOrAdminId;
        let attendUserId = req.body.attendUserId;
        let time = req.body.time;
        let promise = new Promise((resolve, reject) => {
            // 先查一下是否已经存在关注映射
            MysqlBase(`select * from usersAttend where userId = ${userId} and attendUserId = ${attendUserId};`,[], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    if (data.length == 0) {
                        // 不存在就添加
                        // 数据库操作语句
                        let sql = 'insert into usersAttend (userId,attendUserId,time) values (?,?,?);';
                        let sqlArr = [userId, attendUserId, time];
                        // 数据库查询
                        MysqlBase(sql, sqlArr, (_err, data) => {
                            if (_err) {
                                reject(_err);
                            } else {
                                let result = data[0];
                                resolve(result);
                            }
                        })
                    } else {
                        // 存在的话就返回已存在
                        resolve(data);
                    }
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            if (result) {
                //  已经存在关注关系
                res.json({
                    status: '200',
                    msg: '已经存在关注关系',
                    data: result
                })
            } else {
                res.json({
                    status: '200',
                    msg: '关注成功',
                    data: result
                })
            }

        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 取消关注
route.post('/userUnattend', async (req, res) => {
    console.log('get:/userUnattend')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 获取被删除的关注
        let userId = result.userOrAdminId;
        let attendUserId = req.body.attendUserId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `delete from usersAttend where attendUserId = ${attendUserId} and userId = ${userId};`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data[0];
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: '取消关注成功',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 返回指定用户关注列表
route.post('/userAttendList', async (req, res) => {
    console.log('get:/userAttendList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 获取一个用户的关注用户信息列表
        let userId = req.body.userId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from users where userId in (select attendUserId from usersAttend where userId = ${userId}) ;`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得本用户是否关注了指定ID的用户
route.post('/userAttendFlag', async (req, res) => {
    console.log('post:/userAttendFlag')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // token验证通过
        let userId = result.userOrAdminId;
        let attendUserId = req.body.attendUserId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from usersAttend where userId = ${userId} and  attendUserId = ${attendUserId}`;
            let sqlArr = [];
            let result = {};
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    if (data.length == 0) {
                        result['userCollectFlag'] = false;
                    } else {
                        result['userCollectFlag'] = true;
                    }
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
//关注当前用户的所有人
route.get('/userFansList',async (req,res)=>{
    console.log('get:/userFansList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        //token验证通过
        
        
    })
})
module.exports = route;

