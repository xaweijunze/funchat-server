const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const testToken = require('../utils/tokenCheck');

// 删除评论
route.post('/adReviewDelete', async (req, res) => {
    console.log('post:/adReviewDelete')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, false).then(result => {
        // 获取被删除的评论ID
        let reviewId = req.body.reviewId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `delete from reviews where reviewId = ${reviewId};`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data[0];
                    resolve(result);
                }
            })
            // 评论数自减
            MysqlBase(`update articles set reviewNum = reviewNum - 1 WHERE articleId = ${articleId};`, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 返回所有评论
route.post('/adReviewList', async (req, res) => {
    console.log('post:/adReviewList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证管理员
    testToken(token, false).then(result => {
        // 获取一个文章的全部评论
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from reviews ;`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
module.exports = route;
