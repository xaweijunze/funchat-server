


const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const moment = require('moment');// 生成token的框架
const jwt = require('jwt-simple');// JWT(JSON Web Token) 编码解码模块
const testToken = require('../utils/tokenCheck');
// 管理员登录接口
route.post('/adminLogin', async (req, res) => {
    console.log('post:/adminLogin');
    // 从请求体里获取数据
    let email = req.body.email;
    let password = req.body.password;
    // 数据库操作
    let promise = new Promise((resolve, reject) => {
        let sql = `select * from admins where email = '${email}' ;`
        MysqlBase(sql, [], (_err, data) => {
            if (_err) {
                // 查询出错
                reject(_err);
            } else {
                // 查询得到结果
                // 可能查不到说明邮箱错误
                if (JSON.stringify(data) == "[]") {
                    reject(0);
                } else {
                    // 查到了有结果
                    console.log(data);
                    let result = [data[0].adminId, data[0].email, data[0].password];
                    resolve(result);
                }
            }
        })
    })
    promise.then(result => {
        if (result[2] == password) {
            // 登陆成功
            const token = jwt.encode({
                iss: result[0], //签发的管理员ID标识
                exp: moment().add(7, "day").valueOf() //token 过去时间 7天
            }, 'itcast'); //
            res.json({
                status: "200",
                msg: '登陆成功',
                data: {
                    result,
                    token
                }
            })
        } else {
            // 密码错误
            res.json({
                status: "200",
                msg: '密码错误',
                data: ""
            })
        }
    }, err => {
        if (err) {
            // 数据库出错
            res.json({
                status: "409",
                msg: '数据库错误',
                data: err
            })
        } else {
            // 邮箱错误
            res.json({
                status: "200",
                msg: '邮箱错误',
                data: err
            })
        }

    })

})
// 新增管理员接口
route.post('/adminCreate', async (req, res) => {
    console.log('post:/adminCreate');
    const token = req.get('x-access-token');
    // 数据库操作
    testToken(token, false).then(result => {

        // 从请求体里获取数据
        let email = req.body.email;
        let password = req.body.password;
        let sql = 'insert into admins (email,password) values (?,?);';
        let sqlArr = [email, password];
        let promise = new Promise((resolve, reject) => {
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    // 查询出错
                    reject(_err);
                } else {
                    // 查询得到结果

                    resolve(data);
                }
            })
        })
        promise.then(result => {
            res.json({
                status: "200",
                msg: '创建成功！',
                data: result
            })
        }, err => {

            res.json({
                status: "200",
                msg: '邮箱已存在！',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })


})
// token验证
route.get('/tokenCheck', async (req, res) => {
    console.log('get:/tokenCheck');
    const token = req.get('x-access-token');
    console.log(token);
    testToken(token, false).then(result => {
        // token验证通过

        res.json({
            status: '200',
            msg: 'success',
            data: result
        })
    }, err => {
        //验证失败
        res.json({
            status: '401',
            msg: 'fail',
            data: err
        })
    })
})

// 删除管理员接口
route.post('/adminDelete', async (req, res) => {
    console.log('post:/adminDelete');
    const token = req.get('x-access-token');
    // 数据库操作
    testToken(token, false).then(result => {

        // 从请求体里获取数据
        let adminId = req.body.adminId;
        if (adminId == result.userOrAdminId) {
            res.json({
                status: "200",
                msg: '删除失败，不能删除当前用户！',
                data: result
            })
        } else {
            let sql = `Delete from  admins  where adminId = '${adminId}';`;
            let promise = new Promise((resolve, reject) => {
                MysqlBase(sql, [], (_err, data) => {
                    if (_err) {
                        // 查询出错
                        reject(_err);
                    } else {
                        // 查询得到结果

                        resolve(data);
                    }
                })
            })
            promise.then(result => {
                res.json({
                    status: "200",
                    msg: '删除成功！',
                    data: result
                })
            }, err => {

                res.json({
                    status: "200",
                    msg: '删除失败',
                    data: err
                })
            })
        }


    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })


})
// 获取管理员表的全部信息
route.get('/adminList', async (req, res) => {
    console.log('get:/adminList');
    // 获取token
    const token = req.get('x-access-token');
    // 使用testToken函数验证token
    testToken(token, false).then(result => {
        // 查询全部用户信息
        let promise = new Promise((resolve, reject) => {
            let sql = `select * from admins ;`;
            // 数据库查询
            MysqlBase(sql, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })


})
module.exports = route;
