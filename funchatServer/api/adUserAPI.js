const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const moment = require('moment');// 生成token的框架
const testToken = require('../utils/tokenCheck');
const jwt = require('jwt-simple');// JWT(JSON Web Token) 编码解码模块

// 新增用户接口
route.post('/adUserCreate', async (req, res) => {
    console.log('post:/userCreate');
    // req是从客户端发回的请求体
    let email = req.body.email;
    let password = req.body.password;
    // 数据库操作
    let promise = new Promise((resolve, reject) => {
        // 初始化sql语句
        let sql = 'insert into userIndex (email,password) values (?,?);';
        let sqlArr = [email, password];
        // 访问数据库，将注册信息插入用户索引表userIndex
        MysqlBase(sql, sqlArr, (_err, data) => {
            if (_err) {
                // 插入失败
                reject(_err);
            } else {
                // 插入成功
                const results = data;
                // 将自动生成的userId和输进去的email从userIndex表中取出来
                MysqlBase(`select userId,email from userIndex where email = '${email}';`, [], (_err, data) => {
                    if (_err) {
                        // 查询失败，返回失败信息
                        reject(_err);
                    } else {
                        let arr = [];
                        console.log(data);
                        // 成功将数据传入promise.then
                        arr.push(data[0].userId);
                        arr.push(data[0].email);
                        resolve(arr);
                    }
                });
            }
        })
    });
    promise.then(results => {
        console.log(results);
        // 将新注册的用户的userId和email插入表users用户信息表，数据暂时为null
        let sql = 'insert into users (userId,email) values (?,?);';
        let sqlArr = results;
        MysqlBase(sql, sqlArr, (_err, data) => {
            if (_err) {
                // 插入失败，返回失败信息
                res.json({
                    status: '409',
                    msg: '数据库错误',
                    data: _err
                })
            } else {
                // 插入成功，将邮箱和UserId发回客户端
                res.json({
                    status: '200',
                    msg: 'success',
                    data: sqlArr
                })
            }
        })
    }, err => {
        // 插入失败，返回失败信息
        res.json({
            status: '409',
            msg: '数据库错误',
            data: err
        })
    })
})

// 注销用户
// 注销机制，将原本user删除，将属于本user的信息全都置于一个userId为-1的用户已注销的固定用户上，包括评论、收藏、关注发布的文章
route.post('/adUserDelete', async (req, res) => {
    console.log('get:/userDelete');
    const token = req.get('x-access-token');
    testToken(token, false).then(result => {
        // token验证成功，update数据库
        let userId = req.body.userId;
        // 依次将所有userId 置位 -1，最后删除users中的行 删除  userIndex中的行
        let sqlReview = ` update reviews set userId = -1 where userId = ${userId}; `
        let sqlArticle = ` update articles set userId = -1 where userId = ${userId}; `
        let sqlCollect = ` update usersAttend set userId = -1 where userId = ${userId}; `
        let sqlAttend = ` update usersCollect set userId = -1 where userId = ${userId}; `
        let sqlUser = ` DELETE FROM users where userId = ${userId}; `
        let sqlUserIndex = ` DELETE FROM userIndex where userId = ${userId}; `
        let sqlArr = [sqlReview, sqlArticle, sqlCollect, sqlAttend, sqlUser, sqlUserIndex];
        new Promise((resolve, reject) => {
            let result = [];
            // 依次执行查询操作
            sqlArr.forEach((sqlItem, i) => {
                MysqlBase(sqlItem, [], (_err, data) => {
                    if (_err) {
                        reject(_err);
                    } else {
                        result[i] = data;
                        if (sqlArr.length == result.length) {
                            resolve(result);
                        }
                    }
                })
            })

        }).then(result => {
            res.json({
                status: '200',
                msg: '注销成功',
                data: userId
            })
        }, err => {
            res.json({
                status: '501',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })

    })
})
// 获取指定id的用户信息
route.post('/adUserInfoById', async (req, res) => {
    console.log('get:/adUserInfoById');
    let userId = req.body.userId;
    // 获取token
    const token = req.get('x-access-token');
    // 使用testToken函数验证token
    testToken(token, false).then(result => {
        // 验证通过根据客户端传过来的userId查询用户信息
        let promise = new Promise((resolve, reject) => {
            let sql = `select * from users where userId = ${userId};`;
            // 数据库查询
            MysqlBase(sql, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data[0];
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })


})
// 修改用户信息
route.post('/AdUserInfoUpdate', async (req, res) => {
    console.log('get:/AdUserInfoUpdate');
    // 获取token
    const token = req.get('x-access-token');
    // 使用testToken函数验证token
    testToken(token, false).then(result => {
        // token验证成功，update数据库
        let user = {
            'userId': req.body.userId,
            'userName': req.body.userName,
            'sex': req.body.sex,
            'birthday': req.body.birthday,
            //'header': req.body.header,
            'personInfo': req.body.personInfo,
        };
        new Promise((resolve, reject) => {
            // 全部可修改数据全都更新
            let sql = `update users set userName = '${user.userName}', sex = '${user.sex}' , birthday = '${user.birthday}',personInfo = '${user.personInfo}' where userId = '${user.userId}';`;
            MysqlBase(sql, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    resolve(data);
                }
            })
        }).then(result => {
            res.json({
                status: '200',
                msg: '更新成功',
                data: user
            })
        }, err => {
            res.json({
                status: '501',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })

    })
})
// 获取用户表users的全部信息
route.get('/AdUserInfoList', async (req, res) => {
    console.log('get:/AdUserInfoList');
    // 获取token
    const token = req.get('x-access-token');
    // 使用testToken函数验证token
    testToken(token, false).then(result => {
        // 查询全部用户信息
        let promise = new Promise((resolve, reject) => {
            let sql = `select * from users ;`;
            // 数据库查询
            MysqlBase(sql, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })


})

module.exports = route;