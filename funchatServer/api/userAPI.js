const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const moment = require('moment');// 生成token的框架
const testToken = require('../utils/tokenCheck');
const jwt = require('jwt-simple');// JWT(JSON Web Token) 编码解码模块
//连接测试
route.get('/test', async (req, res) => {
    res.json({
        status: '200',
        msg: 'success',
        data: ""
    });
});
// 用户注册接口
route.post('/userRegister', async (req, res) => {
    console.log('post:/userRegister');
    // req是从客户端发回的请求体
    let email = req.body.email;
    let password = req.body.password;
    // 数据库操作
    let promise = new Promise((resolve, reject) => {
        // 初始化sql语句
        let sql = 'insert into userIndex (email,password) values (?,?);';
        let sqlArr = [email, password];
        // 访问数据库，将注册信息插入用户索引表userIndex
        MysqlBase(sql, sqlArr, (_err, data) => {
            if (_err) {
                // 插入失败
                reject(_err);
            } else {
                // 插入成功
                const results = data;
                // 将自动生成的 userId和输进去的email从userIndex表中取出来
                MysqlBase(`select userId,email from userIndex where email = '${email}';`, [], (_err, data) => {
                    if (_err) {
                        // 查询失败，返回失败信息
                        reject(_err);
                    } else {
                        let arr = [];
                        console.log(data);
                        // 成功将数据传入promise.then
                        arr.push(data[0].userId);
                        arr.push(data[0].email);
                        resolve(arr);
                    }
                });
            }
        })
    });
    promise.then(results => {
        console.log(results);
        // 将新注册的用户的userId和email插入表users用户信息表，数据暂时为null
        let sql = 'insert into users (userId,email) values (?,?);';
        let sqlArr = results;
        MysqlBase(sql, sqlArr, (_err, data) => {
            if (_err) {
                // 插入失败，返回失败信息
                res.json({
                    status: '409',
                    msg: '该邮箱已被注册',
                    data: _err
                })
            } else {
                // 插入成功，将邮箱和UserId发回客户端
                res.json({
                    status: '200',
                    msg: 'success',
                    data: sqlArr
                })
            }
        })
    }, err => {
        // 插入失败，返回失败信息
        res.json({
            status: '409',
            msg: '邮箱已被注册',
            data: err
        })
    })
})
// 用户登录接口
route.post('/userlogin', async (req, res) => {
    console.log('post:/userlogin');
    // 从请求体里获取数据
    let email = req.body.email;
    let password = req.body.password;
    // 数据库操作
    let promise = new Promise((resolve, reject) => {
        let sql = `select * from userIndex where email = '${email}' ;`
        MysqlBase(sql, [], (_err, data) => {
            if (_err) {
                // 查询出错
                reject(_err);
            } else {
                // 查询得到结果
                // 可能查不到说明邮箱未注册
                if (JSON.stringify(data) == "[]") {
                    reject(0);
                } else {
                    // 查到了有结果
                    console.log(data);
                    let result = { 'userId': data[0].userId, 'email': data[0].email, 'password': data[0].password };
                    resolve(result);
                }
            }
        })
    })
    promise.then(result => {
        if (result['password'] == password) {
            // 登陆成功
            const token = jwt.encode({
                iss: result['userId'], //签发的用户ID标识
                exp: moment().add(7, "day").valueOf() //token 过期时间 7天
            }, 'itcast'); //
            res.json({
                status: "200",
                msg: '登陆成功',
                data: {
                    'userInfo': result,
                    token
                }
            })
        } else {
            // 密码错误
            res.json({
                status: "200",
                msg: '密码错误',
                data: ""
            })
        }
    }, err => {
        if (err) {
            // 数据库出错
            res.json({
                status: "409",
                msg: '数据库错误',
                data: err
            })
        } else {
            // 邮箱未注册
            res.json({
                status: "200",
                msg: '该邮箱未注册',
                data: err
            })
        }

    })

})
// 获取用户表users的全部信息
route.get('/userInfoList', async (req, res) => {
    console.log('get:/userInfoList');
    let email = req.body.email;
    let password = req.body.password;
    let sql = 'select * from users;';
    let sqlArr = [];
    // 访问数据库
    MysqlBase(sql, sqlArr, (_err, data) => {
        if (_err) {
            // 访问失败，返回失败错误信息
            res.json({
                status: '404',
                msg: 'fail',
                data: _err
            })
        } else {
            // 访问成功，返回数据
            const results = data;

            res.json({
                status: '200',
                msg: 'success',
                data: results

            })
        }
    })
})
// // 用户注销
// route.get('/userDelete', async (req, res) => {
//     console.log('get:/userCancel');
//     let userId = req.body.userId;
//     let sql = `DELETE FROM users where name = ${userId};`
// })

// 获取指定id的用户信息
route.post('/userInfoById', async (req, res) => {
    console.log('get:/userInfoById');
    let userId = req.body.userId;
    // 获取token
    const token = req.get('x-access-token');
    // 使用testToken函数验证token
    testToken(token, true).then(result => {
        // 验证通过根据客户端传过来的userId查询用户信息
        let promise = new Promise((resolve, reject) => {
            let sql = `select * from users where userId = ${userId};`;
            // 数据库查询
            MysqlBase(sql, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data[0];
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成
            msg = result ? "success" : "用户不存在";
            res.json({
                status: '200',
                msg: msg,
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })


})
// 修改用户信息
route.post('/userInfoUpdate', async (req, res) => {
    console.log('get:/userInfoUpdate');
    // 获取token
    const token = req.get('x-access-token');
    // 使用testToken函数验证token
    testToken(token, true).then(result => {
        // token验证成功，update数据库
        console.log(result);
        let user = {
            'userId': result.userOrAdminId,
            'userName': req.body.userName,
            'sex': req.body.sex,
            'birthday': req.body.birthday,
            'header': req.body.header,
            'personInfo': req.body.personInfo,
        };
        new Promise((resolve, reject) => {
            // 全部可修改数据全都更新
            let sql = `update users set userName = '${user.userName}', sex = '${user.sex}' , birthday = '${user.birthday}' , header = '${user.header}',personInfo = '${user.personInfo}' where userId = '${user.userId}';`;
            MysqlBase(sql, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    resolve(data);
                }
            })
        }).then(result => {
            res.json({
                status: '200',
                msg: '更新成功',
                data: user
            })
        }, err => {
            res.json({
                status: '501',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })

    })
})
// 

module.exports = route;