const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const testToken = require('../utils/tokenCheck')

// 新增文章接口
route.post('/articleCreate', async (req, res) => {
    console.log('post:/articleCreate')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 文章信息获取
        let article = {
            'userId': result.userOrAdminId,
            'title': req.body.title,
            'body': req.body.body,
            'tag': req.body.tag,
            'time': req.body.time
        }
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = 'insert into articles (userId,title,body,tag,time) values (?,?,?,?,?);';
            let sqlArr = [article.userId, article.title, article.body, article.tag, article.time];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data[0];
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: article
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得所有审核通过的文章列表 articleCheckedList
route.get('/articleCheckedList', async (req, res) => {
    console.log('get:/articleCheckedList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // token验证通过
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from articles where checkFlag = 1 ;`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = {};
                    result['articleArr'] = data;
                    // 再次查询所有用户的信息
                    MysqlBase(`select * from users;`, [], (_err, data) => {
                        if (_err) {
                            reject(_err);
                        } else {
                            result['userArr'] = data;
                            resolve(result);
                        }
                    })
                }
            })
        })
        // 
        promise.then(result => {
           let article_result = [];
            let articleArr = result['articleArr'];
            let userArr = result['userArr'];

            for(let i = 0;i<articleArr.length;i++){
                let item = articleArr[i];
                let target = findUser(userArr,item.userId)
                item['header'] = target.header;
                item['userName'] = target.userName;
                article_result.push(item);
            }
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: article_result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得用户自己的的文章列表 articleList
route.get('/articleList', async (req, res) => {
    console.log('get:/articleList')
    // 获取token
    const token = req.get('x-access-token');
    console.log(token);
    // token 验证
    testToken(token, true).then(result => {
        // token验证通过
        let userId = result.userOrAdminId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from articles where userId = ${userId};`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = {};
                    result['articleArr'] = data;
                    // 再次查询所有用户的信息
                    MysqlBase(`select * from users;`, [], (_err, data) => {
                        if (_err) {
                            reject(_err);
                        } else {
                            result['userArr'] = data;
                            resolve(result);
                        }
                    })
                }
            })
        })
        // 
        promise.then(result => {
            // 文章信息和对应的作者信息组合
            let article_result = [];
            let articleArr = result['articleArr'];
            let userArr = result['userArr'];

            for(let i = 0;i<articleArr.length;i++){
                let item = articleArr[i];
                let target = findUser(userArr,item.userId)
                item['header'] = target.header;
                item['userName'] = target.userName;
                article_result.push(item);
            }
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: article_result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得指定ID的文章
route.post('/articleById', async (req, res) => {
    console.log('get:/articleById')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // token验证通过
        let articleId = req.body.articleId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from articles where articleId = ${articleId};`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 删除文章接口
route.post('/articleDelete', async (req, res) => {
    console.log('post:/articleDelete')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 文章Id获取
        let articleId = req.body.articleId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `delete from articles where articleId = ${articleId} ;`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 寻找指定Id的用户
function findUser(userArr, userId) {
    for (let i = 0; i < userArr.length; i++) {
        if (userArr[i].userId == userId) {
            return userArr[i];
        }
    }
}
console.log(findUser([{ 'userId': 1 },{ 'userId': 2 }], 1));

module.exports = route;
