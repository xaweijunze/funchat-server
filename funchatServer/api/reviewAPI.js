const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const testToken = require('../utils/tokenCheck');

// 创建评论
route.post('/reviewCreate', async (req, res) => {
    console.log('get:/reviewCreate')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 评论信息获取
        let review = {
            'userId': result.userOrAdminId,
            'articleId': req.body.articleId,
            'body': req.body.body,
            'time': req.body.time
        }
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = 'insert into reviews (userId,articleId,body,time) values (?,?,?,?);';
            let sqlArr = [review.userId, review.articleId, review.body, review.time];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data[0];
                    MysqlBase(`update articles set reviewNum = reviewNum + 1 WHERE articleId = ${review.articleId};`, [], (_err, data) => {
                        if (_err) {
                            reject(_err);
                        } else {
                            resolve(result)
                        }
                    })
                }
            })

        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: review
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 删除评论
route.post('/reviewDelete', async (req, res) => {
    console.log('get:/reviewDelete')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 获取被删除的评论ID
        let userId = result.userOrAdminId;
        let reviewId = req.body.reviewId;
        let articleId = req.body.articleId;

        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `delete from reviews where reviewId = ${reviewId} and userId = ${userId};`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = [data];
                    MysqlBase(`update articles set reviewNum = reviewNum - 1 WHERE articleId = ${articleId};`, [], (_err, data) => {
                        if (_err) {
                            reject(_err);
                        } else {
                            result.push(data)
                            resolve(result);

                        }
                    })
                }
            })

        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 返回所有评论
route.post('/reviewList', async (req, res) => {
    console.log('get:/reviewList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 获取一个文章的全部评论
        let articleId = req.body.articleId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from reviews where articleId = ${articleId} ;`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = {};
                    result['reviewArr'] = data;
                      MysqlBase(`select * from users;`, [], (_err, data) => {
                        if (_err) {
                            reject(_err);
                        } else {
                            result['userArr'] = data;
                            resolve(result);
                        }
                    })
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
           // 文章信息和对应的作者信息组合
            let review_result = [];
            let reviewArr = result['reviewArr'];
            let userArr = result['userArr'];

            for(let i = 0;i<reviewArr.length;i++){
                let item = reviewArr[i];
                let target = findUser(userArr,item.userId)
                item['header'] = target.header;
                item['userName'] = target.userName;
                review_result.push(item);
            }
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: review_result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
function findUser(userArr, userId) {
    for (let i = 0; i < userArr.length; i++) {
        if (userArr[i].userId == userId) {
            return userArr[i];
        }
    }
}
module.exports = route;
