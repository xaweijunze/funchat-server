const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const testToken = require('../utils/tokenCheck');

// 用户文章审核接口
route.post('/adArticleCheck', async (req, res) => {
    console.log('get:/adArticleCheck')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, false).then(result => {
        // token验证通过
        let articleId = req.body.articleId;
        let checkFlag = req.body.checkFlag;
        checkFlag = checkFlag == '1' ? 0 : 1;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `update articles  set checkFlag = ${checkFlag} where articleId = ${articleId};`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得待审核的文章列表 articleUncheck
route.get('/adArticleUncheck', async (req, res) => {
    console.log('get:/adArticleUncheck')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, false).then(result => {
        // token验证通过
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from articles where checkFlag = 0;`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得全部的文章列表 adArticleList
route.get('/adArticleList', async (req, res) => {
    console.log('get:/adArticleList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, false).then(result => {
        // token验证通过
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from articles;`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 删除文章
route.post('/adArticleDelete', async (req, res) => {
    console.log('get:/adArticleDelete')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, false).then(result => {
        // token验证通过
        let articleId = req.body.articleId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `Delete from articles where articleId = ${articleId};`;
            let sqlArr = [];
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
module.exports = route;
