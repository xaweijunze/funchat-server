const express = require('express');
const route = express.Router();
const MysqlBase = require('../utils/mysqlPool'); //引入mysql数据库连接池和查询方法
const moment = require('moment');// 生成token的框架
const testToken = require('../utils/tokenCheck');

// 新增收藏
route.post('/articleCollect', async (req, res) => {
    console.log('get:/articleCollect')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 收藏文章ID获取
        let userId = result.userOrAdminId;
        let articleId = req.body.articleId;
        let time = req.body.time;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            MysqlBase(`select * from usersCollect where userId = ${userId} and articleId = ${articleId};`, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    if (data.length == 0) {
                        let sql = 'insert into usersCollect (userId,articleId,time) values (?,?,?);';
                        let sqlArr = [userId, articleId, time];
                        // 数据库查询
                        MysqlBase(sql, sqlArr, (_err, data) => {
                            if (_err) {
                                reject(_err);
                            } else {
                                let result = data[0];
                                resolve(result);
                            }
                        })
                        MysqlBase(`update articles set collectNum = collectNum + 1 WHERE articleId = ${articleId};`, [], (_err, data) => {
                            if (_err) {
                                reject(_err);
                            } else {
                            }
                        })
                    } else {
                        resolve(data);
                    }

                }
            })

        })
        // 
        promise.then(result => {
            // 查询成功
            if (result) {
                res.json({
                    status: '200',
                    msg: '用户已收藏该文章',
                    data: result
                })
            } else {
                res.json({
                    status: '200',
                    msg: '收藏文章成功',
                    data: result
                })
            }

        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 取消收藏
route.post('/articleUncollect', async (req, res) => {
    console.log('get:/articleUncollect')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 获取被删除收藏文章的ID 
        let userId = result.userOrAdminId;
        let articleId = req.body.articleId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            MysqlBase(`select * from usersCollect where userId = ${userId} and articleId = ${articleId};`, [], (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    if (data.length == 1) {
                        let sql = `delete from usersCollect where articleId = ${articleId} and userId = ${userId};`;
                        let sqlArr = [];
                        // 数据库操作
                        MysqlBase(sql, sqlArr, (_err, data) => {
                            if (_err) {
                                reject(_err);
                            } else {
                                let result = data[0];
                                resolve(1);
                            }
                        })
                        MysqlBase(`update articles set collectNum = collectNum - 1 WHERE articleId = ${articleId};`, [], (_err, data) => {
                            if (_err) {
                                reject(_err);
                            } else {

                            }
                        })
                    } else {
                        resolve(0)
                    }
                }
            })


        })
        // 
        promise.then(result => {
            // 查询成功
            if (result) {
                res.json({
                    status: '200',
                    msg: '取消收藏成功',
                    data: result
                })
            } else {
                res.json({
                    status: '200',
                    msg: '用户并未收藏该文章',
                    data: result
                })
            }

        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 返回指定用户收藏列表
route.post('/articleCollectList', async (req, res) => {
    console.log('get:/articleCollectList')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // 获取一个用户的收藏文章信息列表
        let userId = req.body.userId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from articles where articleId in (select articleId from usersCollect where userId = ${userId}) ;`;
            let sqlArr = [];
            // 数据库操作
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    let result = data;
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
// 获得 用户是否收藏了指定ID的文章
route.post('/articleCollectFlag', async (req, res) => {
    console.log('get:/articleCollectFlag')
    // 获取token
    const token = req.get('x-access-token');
    // token 验证
    testToken(token, true).then(result => {
        // token验证通过
        let userId = req.body.userId;
        let articleId = req.body.articleId;
        let promise = new Promise((resolve, reject) => {
            // 数据库操作语句
            let sql = `select * from usersCollect where userId = ${userId} and articleId = ${articleId}`;
            let sqlArr = [];
            let result = {};
            // 数据库查询
            MysqlBase(sql, sqlArr, (_err, data) => {
                if (_err) {
                    reject(_err);
                } else {
                    if (data.length == 0) {
                        result['userCollectFlag'] = false;
                    } else {
                        result['userCollectFlag'] = true;
                    }
                    resolve(result);
                }
            })
        })
        // 
        promise.then(result => {
            // 查询成功
            res.json({
                status: '200',
                msg: 'success',
                data: result
            })
        }, err => {
            // 查询失败
            res.json({
                status: '401',
                msg: '数据库错误',
                data: err
            })
        })
    }, err => {
        // token验证失败，失败信息存放在err里
        res.json({
            status: '401',
            msg: 'token验证失败',
            data: err
        })
    })
})
module.exports = route;
