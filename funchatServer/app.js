const express = require('express');
const app = express();
const userAPI = require('./api/userAPI');
const articleAPI = require('./api/articleAPI');
const reviewAPI = require('./api/reviewAPI');
const appendAPI = require('./api/appendAPI');
const collectAPI = require('./api/collectAPI')
const adminAPI = require('./api/adminAPI')
const adUserAPI = require('./api/adUserAPI')
const adArticleAPI = require('./api/adArticleAPI')

app.all('*', function (req, res, next) {

    //设置请求头
    //允许所有来源访问
    res.header('Access-Control-Allow-Origin', '*')

    //用于判断request来自ajax还是传统请求
    res.header("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,x-access-token");
    //允许访问的方式
    res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
    //修改程序信息与版本
    // res.header('X-Powered-By', ' 3.2.1')
    //内容类型：如果是post请求必须指定这个属性
    res.header('Content-Type', 'application/json;charset=utf-8')
    // console.log(req);
    if (req.method === 'OPTIONS') {
        res.json({
            status: '200',
            msg: 'success',
            data: 'results'
        })
        return
    }
    next()
})
// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }))

// parse application/json
app.use(express.json());
app.use(userAPI);
app.use(articleAPI);
app.use(reviewAPI);
app.use(appendAPI);
app.use(collectAPI);
app.use(adminAPI);
app.use(adUserAPI);
app.use(adArticleAPI);

app.listen(3007, function () {
    console.log('express 正在监听 3007 端口')
})