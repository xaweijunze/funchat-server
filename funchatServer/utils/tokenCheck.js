const MysqlBase = require('./mysqlPool'); //引入mysql数据库连接池和查询方法
const jwt = require('jwt-simple');// JWT(JSON Web Token) 编码解码模块
const moment = require('moment');// 生成token的框架
var testToken = function (token,userOrAdmin) {
    // userOrAdmin是判断是用户还是管理员的token验证 true 就是 user 否则就是管理员
    return new Promise((resolve, reject) => {
        // token为空返回
        if (!token) {
            reject({
                'key': 0,
                'msg': '无token信息'
            })
        }
        try {
            const decodedToken = jwt.decode(token, 'itcast');//使用key解密token
            // 如果过期时间小于当前时间，说明已过期
            if (decodedToken.exp < moment().valueOf()) {
                reject({
                    'key': 1,
                    'msg': 'token已过期'
                })
            } else {
                // token在有效时间内,获得userId
                let userOrAdminId = decodedToken.iss;
                // 查询数据库测试userId的正确性
                let sql =userOrAdmin? `select * from users where userId = ${userOrAdminId}` :`select * from admins where adminId = ${userOrAdminId}`;
                MysqlBase(sql, [], (_err, data) => {
                    if (_err) {
                        // 数据库错误
                        reject({
                            'key': 2,
                            'msg': '数据库查询错误'
                        })
                    } else {
                        // 没有查到结果，说明解密出来的userId不存在，token错误
                        if (JSON.stringify(data) == "[]") {
                            reject({
                                'key': 3,
                                'msg': 'token信息错误'
                            })
                        } else {
                            // 结果正确
                            resolve({
                                'key': 4,
                                'msg': 'token信息正确',
                                userOrAdminId: userOrAdminId
                            })
                        }
                    }
                })
            }
        } catch {
            // token有误解码出现问题会跳转到这里
            reject({
                'key': 3,
                'msg': 'token信息错误'
            })
        }
    })
}
module.exports = testToken;
