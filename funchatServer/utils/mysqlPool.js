// 数据库连接池
var mysql = require("mysql");

// 数据库操作
// 创建连接池：创建多个连接、复用与分发链接
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '123456',
    database: 'FunChat'
})
// sql是sql语句，options是要传进去的数据数组 ，callback是查询结束调用的回调函数
var query = function (sql, options, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null, null);
        } else {
            connection.query(sql, options, function (err, results) {
                callback(err, results); //结果回调
                connection.release()  //释放连接资源
            })
        }
    })
}
module.exports = query;  //导出模块
